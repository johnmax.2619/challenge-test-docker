FROM node:10.24.0-alpine

RUN apk update && apk add openssl bash

ENV APP_HOME=/app

RUN mkdir $APP_HOME

WORKDIR $APP_HOME

COPY . $APP_HOME/

RUN npm install

ENTRYPOINT [ "npm", "start" ]